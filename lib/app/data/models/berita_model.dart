import 'dart:convert';

BeritaModel beritaModelFromJson(String str) =>
    BeritaModel.fromJson(json.decode(str));

String beritaModelToJson(BeritaModel data) => json.encode(data.toJson());

class BeritaModel {
  BeritaModel({
    this.id,
    this.title,
    this.description,
    this.createdDate,
  });

  String? id;
  String? title;
  String? description;
  String? createdDate;

  factory BeritaModel.fromJson(Map<String, dynamic> json) => BeritaModel(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        createdDate: json["createdDate"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "createdDate": createdDate,
      };
}
