import 'dart:convert';

PublikasiModel publikasiModelFromJson(String str) =>
    PublikasiModel.fromJson(json.decode(str));

String publikasiModelToJson(PublikasiModel data) => json.encode(data.toJson());

class PublikasiModel {
  PublikasiModel({
    this.id,
    this.title,
    this.description,
    this.createdDate,
    this.fileUrl,
  });

  String? id;
  String? title;
  String? description;
  String? createdDate;
  String? fileUrl;

  factory PublikasiModel.fromJson(Map<String, dynamic> json) => PublikasiModel(
        id: json["id"],
        title: json["title"],
        description: json["description"],
        createdDate: json["createdDate"],
        fileUrl: json["fileUrl"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "description": description,
        "createdDate": createdDate,
        "fileUrl": fileUrl,
      };
}
