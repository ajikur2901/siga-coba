part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const BERITA = _Paths.BERITA;
  static const INFOGRAFIK = _Paths.INFOGRAFIK;
  static const KONSELING = _Paths.KONSELING;
  static const PROFIL = _Paths.PROFIL;
  static const BERITA_DETAIL = _Paths.BERITA_DETAIL;
  static const PUBLIKASI_DETAIL = _Paths.PUBLIKASI_DETAIL;
  static const PROFIL_EDIT = _Paths.PROFIL_EDIT;
  static const LOGIN = _Paths.LOGIN;
  static const KONSELING_DAFTAR = _Paths.KONSELING_DAFTAR;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const BERITA = '/berita';
  static const INFOGRAFIK = '/infografik';
  static const KONSELING = '/konseling';
  static const PROFIL = '/profil';
  static const BERITA_DETAIL = '/berita-detail';
  static const PUBLIKASI_DETAIL = '/publikasi-detail';
  static const PROFIL_EDIT = '/profil-edit';
  static const LOGIN = '/login';
  static const KONSELING_DAFTAR = '/konseling-daftar';
}
