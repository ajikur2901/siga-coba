import 'package:get/get.dart';

import '../modules/berita/bindings/berita_binding.dart';
import '../modules/berita/views/berita_view.dart';
import '../modules/berita_detail/bindings/berita_detail_binding.dart';
import '../modules/berita_detail/views/berita_detail_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/infografik/bindings/infografik_binding.dart';
import '../modules/infografik/views/infografik_view.dart';
import '../modules/konseling/bindings/konseling_binding.dart';
import '../modules/konseling/views/konseling_view.dart';
import '../modules/konseling_daftar/bindings/konseling_daftar_binding.dart';
import '../modules/konseling_daftar/views/konseling_daftar_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/profil/bindings/profil_binding.dart';
import '../modules/profil/views/profil_view.dart';
import '../modules/profil_edit/bindings/profil_edit_binding.dart';
import '../modules/profil_edit/views/profil_edit_view.dart';
import '../modules/publikasi_detail/bindings/publikasi_detail_binding.dart';
import '../modules/publikasi_detail/views/publikasi_detail_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: _Paths.BERITA,
      page: () => BeritaView(),
      binding: BeritaBinding(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: _Paths.INFOGRAFIK,
      page: () => InfografikView(),
      binding: InfografikBinding(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: _Paths.KONSELING,
      page: () => KonselingView(),
      binding: KonselingBinding(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: _Paths.PROFIL,
      page: () => ProfilView(),
      binding: ProfilBinding(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: _Paths.BERITA_DETAIL,
      page: () => BeritaDetailView(),
      binding: BeritaDetailBinding(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: _Paths.PUBLIKASI_DETAIL,
      page: () => PublikasiDetailView(),
      binding: PublikasiDetailBinding(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: _Paths.PROFIL_EDIT,
      page: () => ProfilEditView(),
      binding: ProfilEditBinding(),
      transition: Transition.rightToLeft,
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.KONSELING_DAFTAR,
      page: () => KonselingDaftarView(),
      binding: KonselingDaftarBinding(),
    ),
  ];
}
