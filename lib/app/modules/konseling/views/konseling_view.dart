import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';

import 'package:get/get.dart';
import 'package:siga/app/routes/app_pages.dart';

import '../controllers/konseling_controller.dart';

class KonselingView extends GetView<KonselingController> {
  final konselingController = Get.find<KonselingController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Konseling'),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.grey.shade100,
        child: Column(
          children: [
            Container(
              width: Get.width,
              padding: const EdgeInsets.all(20),
              child: ElevatedButton(
                onPressed: () => Get.toNamed(Routes.KONSELING_DAFTAR),
                child: const Text("Daftar Konseling"),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ),
            const Divider(),
            Container(
              width: Get.width,
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Jadwal Konseling",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text(
                                  "25",
                                  style: TextStyle(
                                    fontSize: 50,
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  "September",
                                  style: TextStyle(
                                    color: Colors.grey.shade700,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "P2TPA KK Rekso Dyah Utami",
                                  style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                const Text(
                                  "Pukul 13.00",
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Obx(() {
                                      return FlutterSwitch(
                                        activeColor: Colors.red,
                                        padding: 2,
                                        toggleSize: 6,
                                        height: 10,
                                        width: 20,
                                        value: konselingController.remind.value,
                                        onToggle:
                                            konselingController.toggleRemindMe,
                                      );
                                    }),
                                    const Text(
                                      "Ingatkan Saya",
                                      style: TextStyle(
                                        fontSize: 8,
                                      ),
                                    ),
                                    Spacer(),
                                    ElevatedButton.icon(
                                      onPressed: () {},
                                      icon: const Icon(
                                        Icons.location_on,
                                        size: 10,
                                      ),
                                      label: const Text(
                                        "Lokasi",
                                        style: TextStyle(
                                          fontSize: 10,
                                        ),
                                      ),
                                      style: ElevatedButton.styleFrom(
                                        minimumSize: Size(80, 30),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text(
                                  "25",
                                  style: TextStyle(
                                    fontSize: 50,
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  "September",
                                  style: TextStyle(
                                    color: Colors.grey.shade700,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Hotline BP3AP2",
                                  style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                  ),
                                ),
                                const Text(
                                  "Pukul 13.00",
                                  style: TextStyle(
                                    color: Colors.grey,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Obx(() {
                                      return FlutterSwitch(
                                        activeColor: Colors.red,
                                        padding: 2,
                                        toggleSize: 6,
                                        height: 10,
                                        width: 20,
                                        value: konselingController.remind.value,
                                        onToggle:
                                            konselingController.toggleRemindMe,
                                      );
                                    }),
                                    const Text(
                                      "Ingatkan Saya",
                                      style: TextStyle(
                                        fontSize: 8,
                                      ),
                                    ),
                                    Spacer(),
                                    ElevatedButton.icon(
                                      onPressed: () {},
                                      icon: const Icon(
                                        Icons.message,
                                        size: 10,
                                      ),
                                      label: const Text(
                                        "Chat",
                                        style: TextStyle(
                                          fontSize: 10,
                                        ),
                                      ),
                                      style: ElevatedButton.styleFrom(
                                        minimumSize: Size(80, 30),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            const Divider(),
            Container(
              width: Get.width,
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Riwayat Konseling",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.red,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        children: const [
                          Expanded(
                            flex: 2,
                            child: Text(
                              "13 Desember 2021",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Text(
                              "P2TPA KK Rekso Dyah Utami",
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        children: const [
                          Expanded(
                            flex: 2,
                            child: Text(
                              "13 Desember 2021",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.red,
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 3,
                            child: Text(
                              "Hotline BP3AP2",
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
