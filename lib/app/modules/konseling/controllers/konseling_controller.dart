import 'package:get/get.dart';

class KonselingController extends GetxController {
  var remind = true.obs;

  void toggleRemindMe(value) {
    remind.value = value;
  }
}
