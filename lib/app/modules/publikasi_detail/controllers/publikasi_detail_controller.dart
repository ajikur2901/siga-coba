import 'package:get/get.dart';
import 'package:siga/app/data/models/publikasi_model.dart';
import 'package:url_launcher/url_launcher.dart';

class PublikasiDetailController extends GetxController {
  final PublikasiModel arguments = Get.arguments;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> launchInBrowser() async {
    Uri url = Uri.parse(arguments.fileUrl ?? "");
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw 'Could not launch $url';
    }
  }
}
