import 'package:get/get.dart';

import '../controllers/publikasi_detail_controller.dart';

class PublikasiDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PublikasiDetailController>(
      () => PublikasiDetailController(),
    );
  }
}
