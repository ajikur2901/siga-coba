import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../controllers/publikasi_detail_controller.dart';

class PublikasiDetailView extends GetView<PublikasiDetailController> {
  final detailPublikasiController = Get.put(PublikasiDetailController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              DateFormat("d MMMM yyyy").format(
                DateTime.parse(
                  detailPublikasiController.arguments.createdDate ??
                      DateTime.now().toIso8601String(),
                ),
              ),
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
            Text(
              detailPublikasiController.arguments.title ?? "",
              style: TextStyle(
                color: Colors.red.shade900,
                fontWeight: FontWeight.bold,
                fontSize: 25,
              ),
            ),
            Text(
              detailPublikasiController.arguments.description ?? "",
              textAlign: TextAlign.justify,
            ),
            ElevatedButton.icon(
              onPressed: () => detailPublikasiController.launchInBrowser(),
              icon: Icon(Icons.cloud_download),
              label: Text("Download"),
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
