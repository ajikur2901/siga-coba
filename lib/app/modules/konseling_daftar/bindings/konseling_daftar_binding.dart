import 'package:get/get.dart';

import '../controllers/konseling_daftar_controller.dart';

class KonselingDaftarBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<KonselingDaftarController>(
      () => KonselingDaftarController(),
    );
  }
}
