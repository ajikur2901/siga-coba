import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:siga/app/data/models/city_model.dart';

class KonselingDaftarController extends GetxController {
  final kokab = [].obs;
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  @override
  void onInit() {
    fetchKoKab();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  Future<void> fetchKoKab() async {
    CollectionReference beritaCollection = firestore.collection("city");

    QuerySnapshot querySnapshot = await beritaCollection.get();

    List<CityModel> listKoKab = [];
    final List<dynamic> allData = querySnapshot.docs
        .map((doc) => {"data": doc.data(), "id": doc.id})
        .toList();

    allData.forEach((element) {
      listKoKab.add(
        CityModel(
          id: element["id"] ?? "id",
          name: element["data"]["name"],
        ),
      );
    });

    kokab.value = listKoKab;

    kokab.refresh();
  }
}
