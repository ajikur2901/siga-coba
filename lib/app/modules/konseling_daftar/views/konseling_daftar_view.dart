import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:siga/app/data/models/city_model.dart';

import '../controllers/konseling_daftar_controller.dart';

class KonselingDaftarView extends GetView<KonselingDaftarController> {
  static const double _kItemExtent = 32.0;
  final konselingDaftarController = Get.put(KonselingDaftarController());

  final _jenisKonseling = [
    "Koseling 1",
    "Koseling 2",
    "Koseling 3",
    "Koseling 4",
    "Koseling 5"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Daftar Konseling'),
        centerTitle: true,
      ),
      body: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Tanggal Konseling",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(
                      readOnly: true,
                      onTap: () {
                        showCupertinoModalPopup(
                          context: context,
                          builder: (BuildContext context) {
                            return Container(
                              height: Get.height * 0.4,
                              padding: EdgeInsets.only(top: 6),
                              margin: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom,
                              ),
                              color: CupertinoColors.white,
                              child: SafeArea(
                                child: CupertinoDatePicker(
                                  mode: CupertinoDatePickerMode.date,
                                  onDateTimeChanged: (DateTime newDate) {},
                                  use24hFormat: true,
                                  minimumYear: DateTime.now().year,
                                  maximumYear: DateTime.now().year + 1,
                                  dateOrder: DatePickerDateOrder.dmy,
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Expanded(
                      flex: 2,
                      child: Text(
                        "Jam Konseling",
                        style: TextStyle(
                          color: Colors.red,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: CupertinoTextField(
                        readOnly: true,
                        onTap: () {
                          showCupertinoModalPopup(
                            context: context,
                            builder: (BuildContext context) {
                              return Container(
                                height: Get.height * 0.4,
                                padding: EdgeInsets.only(top: 6),
                                margin: EdgeInsets.only(
                                  bottom:
                                      MediaQuery.of(context).viewInsets.bottom,
                                ),
                                color: CupertinoColors.white,
                                child: SafeArea(
                                  top: false,
                                  child: CupertinoDatePicker(
                                    mode: CupertinoDatePickerMode.time,
                                    onDateTimeChanged: (DateTime newDate) {},
                                    use24hFormat: true,
                                    dateOrder: DatePickerDateOrder.dmy,
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      ),
                    ),
                    const Expanded(
                      flex: 3,
                      child: Text(
                        "Jam Konseling 09.00 - 15.00",
                        style: TextStyle(fontSize: 9),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Jenis Konseling",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(
                      readOnly: true,
                      placeholder: "Pilih salah satu",
                      onTap: () {
                        showCupertinoModalPopup(
                          context: context,
                          builder: (BuildContext context) {
                            return Container(
                              height: Get.height * 0.4,
                              padding: EdgeInsets.only(top: 6),
                              margin: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom,
                              ),
                              color: CupertinoColors.white,
                              child: SafeArea(
                                top: false,
                                child: CupertinoPicker(
                                  magnification: 1.22,
                                  squeeze: 1.2,
                                  useMagnifier: true,
                                  itemExtent: _kItemExtent,
                                  onSelectedItemChanged: (int selectedItem) {},
                                  children: List<Widget>.generate(
                                    _jenisKonseling.length,
                                    (int index) {
                                      return Center(
                                        child: Text(
                                          _jenisKonseling[index],
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "Kota / Kabupaten",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(
                      readOnly: true,
                      placeholder: "Pilih salah satu",
                      onTap: () {
                        showCupertinoModalPopup(
                          context: context,
                          builder: (BuildContext context) {
                            return Container(
                              height: Get.height * 0.4,
                              padding: const EdgeInsets.only(top: 6),
                              margin: EdgeInsets.only(
                                bottom:
                                    MediaQuery.of(context).viewInsets.bottom,
                              ),
                              color: CupertinoColors.white,
                              child: SafeArea(
                                top: false,
                                child: CupertinoPicker(
                                  magnification: 1.22,
                                  squeeze: 1.2,
                                  useMagnifier: true,
                                  itemExtent: _kItemExtent,
                                  onSelectedItemChanged: (int selectedItem) {},
                                  children: List<Widget>.generate(
                                    konselingDaftarController.kokab.length,
                                    (int index) {
                                      CityModel kokab =
                                          konselingDaftarController
                                              .kokab[index];
                                      return Center(
                                        child: Text(
                                          kokab.name,
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: ElevatedButton(
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          primary: Colors.grey,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text("Batal"),
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      flex: 2,
                      child: ElevatedButton(
                        onPressed: () {},
                        child: const Text("Daftar Konseling"),
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
