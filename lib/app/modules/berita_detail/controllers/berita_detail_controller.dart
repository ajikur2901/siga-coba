import 'package:get/get.dart';
import 'package:siga/app/data/models/berita_model.dart';

class BeritaDetailController extends GetxController {
  final BeritaModel arguments = Get.arguments;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
