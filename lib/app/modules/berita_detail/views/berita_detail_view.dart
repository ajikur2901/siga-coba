import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../controllers/berita_detail_controller.dart';

class BeritaDetailView extends GetView<BeritaDetailController> {
  final detailBeritaController = Get.put(BeritaDetailController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              DateFormat("d MMMM yyyy").format(
                DateTime.parse(
                  detailBeritaController.arguments.createdDate ??
                      DateTime.now().toIso8601String(),
                ),
              ),
              style: const TextStyle(
                color: Colors.grey,
              ),
            ),
            Text(
              detailBeritaController.arguments.title ?? "",
              style: TextStyle(
                color: Colors.red.shade900,
                fontWeight: FontWeight.bold,
                fontSize: 25,
              ),
            ),
            Text(
              detailBeritaController.arguments.description ?? "",
              textAlign: TextAlign.justify,
            ),
          ],
        ),
      ),
    );
  }
}
