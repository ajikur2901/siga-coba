import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:siga/app/data/models/berita_model.dart';
import 'package:siga/app/data/models/publikasi_model.dart';

class BeritaController extends GetxController {
  final beritaList = [].obs;
  final publikasiList = [].obs;
  FirebaseFirestore firestore = FirebaseFirestore.instance;

  @override
  void onInit() {
    fetchBerita();
    fetchPublikasi();
    super.onInit();
  }

  Future<void> fetchBerita() async {
    CollectionReference beritaCollection = firestore.collection("berita");

    QuerySnapshot querySnapshot = await beritaCollection.get();

    List<BeritaModel> listBerita = [];
    final List<dynamic> allData = querySnapshot.docs
        .map((doc) => {"data": doc.data(), "id": doc.id})
        .toList();

    allData.forEach((element) {
      listBerita.add(
        BeritaModel(
          id: element["id"] ?? "id",
          title: element["data"]["title"],
          description: element["data"]["description"],
          createdDate: element["data"]["createdDate"],
        ),
      );
    });

    beritaList.value = listBerita;

    beritaList.refresh();
  }

  Future<void> fetchPublikasi() async {
    CollectionReference publikasiCollection = firestore.collection("publikasi");

    QuerySnapshot querySnapshot = await publikasiCollection.get();

    List<PublikasiModel> listPublikasi = [];
    final List<dynamic> allData = querySnapshot.docs
        .map((doc) => {"data": doc.data(), "id": doc.id})
        .toList();

    allData.forEach((element) {
      listPublikasi.add(
        PublikasiModel(
          id: element["id"] ?? "id",
          title: element["data"]["title"],
          description: element["data"]["description"],
          createdDate: element["data"]["createdDate"],
          fileUrl: element["data"]["fileUrl"],
        ),
      );
    });

    publikasiList.value = listPublikasi;

    publikasiList.refresh();
  }
}
