import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'package:siga/app/data/models/berita_model.dart';
import 'package:siga/app/data/models/publikasi_model.dart';
import 'package:siga/app/routes/app_pages.dart';

import '../controllers/berita_controller.dart';

class BeritaView extends GetView<BeritaController> {
  final beritaController = Get.put(BeritaController());

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Portal Informasi'),
          centerTitle: true,
          bottom: const TabBar(
            tabs: [
              Tab(text: "Berita"),
              Tab(text: "Publikasi"),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            Container(
              child: ListView.builder(
                itemCount: beritaController.beritaList.value.length,
                itemBuilder: (context, index) {
                  final BeritaModel berita =
                      beritaController.beritaList.value[index];
                  return Column(
                    children: [
                      ListTile(
                        onTap: () => Get.toNamed(
                          Routes.BERITA_DETAIL,
                          arguments: berita,
                        ),
                        title: Text(
                          berita.title ?? "",
                          style: TextStyle(
                            color: Colors.red.shade900,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        subtitle: Text(
                          // berita.createdDate ?? "",
                          DateFormat("d MMMM yyyy").format(
                            DateTime.parse(
                              berita.createdDate ??
                                  DateTime.now().toIso8601String(),
                            ),
                          ),
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      Divider(),
                    ],
                  );
                },
              ),
            ),
            Container(
              child: ListView.builder(
                itemCount: beritaController.publikasiList.value.length,
                itemBuilder: (context, index) {
                  final PublikasiModel publikasi =
                      beritaController.publikasiList.value[index];
                  return Column(
                    children: [
                      ListTile(
                        onTap: () => Get.toNamed(
                          Routes.PUBLIKASI_DETAIL,
                          arguments: publikasi,
                        ),
                        title: Text(
                          publikasi.title ?? "",
                          style: TextStyle(
                            color: Colors.red.shade900,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        subtitle: Text(
                          DateFormat("d MMMM yyyy").format(
                            DateTime.parse(
                              publikasi.createdDate ??
                                  DateTime.now().toIso8601String(),
                            ),
                          ),
                          style: TextStyle(color: Colors.grey),
                        ),
                      ),
                      Divider(),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
