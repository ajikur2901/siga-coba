import 'package:get/get.dart';

import '../controllers/profil_edit_controller.dart';

class ProfilEditBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ProfilEditController>(
      () => ProfilEditController(),
    );
  }
}
