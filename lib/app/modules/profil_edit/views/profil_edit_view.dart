import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/profil_edit_controller.dart';

class ProfilEditView extends GetView<ProfilEditController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: Get.width,
                padding: const EdgeInsets.only(
                  bottom: 20,
                ),
                child: const Text(
                  "Silahkan edit data diri anda",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.red,
                    fontSize: 20,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Nama",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Username",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: const [
                          Text(
                            "Jenis kelamin",
                            style: TextStyle(
                              color: Colors.red,
                            ),
                          ),
                          CupertinoTextField(),
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Tanggal Lahir",
                            style: TextStyle(
                              color: Colors.red,
                            ),
                          ),
                          CupertinoTextField(
                            readOnly: true,
                            onTap: () {
                              showCupertinoModalPopup(
                                context: context,
                                builder: (BuildContext context) {
                                  return Container(
                                    height: Get.height * 0.4,
                                    padding: EdgeInsets.only(top: 6),
                                    margin: EdgeInsets.only(
                                      bottom: MediaQuery.of(context)
                                          .viewInsets
                                          .bottom,
                                    ),
                                    color: CupertinoColors.white,
                                    child: SafeArea(
                                      top: false,
                                      child: CupertinoDatePicker(
                                        mode: CupertinoDatePickerMode.date,
                                        onDateTimeChanged:
                                            (DateTime newDate) {},
                                        use24hFormat: true,
                                        minimumYear: 1900,
                                        maximumYear: DateTime.now().year,
                                        dateOrder: DatePickerDateOrder.dmy,
                                      ),
                                    ),
                                  );
                                },
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Kota / Kabupaten Domisili",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Kecamatan Domisili",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Email",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "No. handphone",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Password",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Ulangi password",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                    CupertinoTextField(),
                  ],
                ),
              ),
              ElevatedButton(
                onPressed: () {},
                child: const Text("Simpan"),
                style: ElevatedButton.styleFrom(
                  fixedSize: Size(Get.width, 0),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
