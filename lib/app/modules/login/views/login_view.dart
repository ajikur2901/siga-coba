import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:siga/app/controllers/auth_controller.dart';
import 'package:siga/app/routes/app_pages.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    final authController = Get.put(AuthController());

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.white,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark,
      ),
    );

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset("assets/images/data-report.png"),
              const CupertinoTextField(
                prefix: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.person,
                    color: Colors.red,
                  ),
                ),
                placeholder: "Masukkan username anda",
              ),
              const SizedBox(
                height: 20,
              ),
              const CupertinoTextField(
                prefix: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.key,
                    color: Colors.red,
                  ),
                ),
                suffix: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.visibility,
                    color: Colors.red,
                  ),
                ),
                placeholder: "Masukkan password anda",
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  fixedSize: Size(Get.width, 0),
                ),
                child: const Text("Login"),
              ),
              const SizedBox(
                height: 20,
              ),
              RichText(
                text: TextSpan(
                  children: [
                    const TextSpan(
                      text: "Belum punya akun?",
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    TextSpan(
                      text: "Daftar disini",
                      style: const TextStyle(
                        color: Colors.red,
                      ),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () => Get.toNamed(Routes.PROFIL_EDIT),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  const Expanded(
                    flex: 3,
                    child: Divider(
                      color: Colors.grey,
                      thickness: 2,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      alignment: Alignment.center,
                      child: const Text("atau"),
                    ),
                  ),
                  const Expanded(
                    flex: 3,
                    child: Divider(
                      color: Colors.grey,
                      thickness: 2,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton.icon(
                onPressed: () => authController.login(),
                icon: Image.asset(
                  "assets/images/google.png",
                  height: 20,
                  width: 20,
                ),
                label: const Text(
                  "Masuk dengan Google",
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  fixedSize: Size(Get.width, 0),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const Text(
                "copyright©2022",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.red,
                ),
              ),
              const Text(
                "Dinas Pemberdayaan Perempuan Perlindungan Anak dan Pengendalian Penduduk",
                textAlign: TextAlign.center,
              ),
              const Text(
                "Daerah Istimewa Yogyakarta",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
