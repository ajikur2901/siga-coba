import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:siga/app/modules/berita/views/berita_view.dart';
import 'package:siga/app/modules/infografik/views/infografik_view.dart';
import 'package:siga/app/modules/konseling/views/konseling_view.dart';
import 'package:siga/app/modules/profil/views/profil_view.dart';
import 'package:siga/app/routes/app_pages.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final homeController = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.red,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.light,
      ),
    );
    return Scaffold(
      body: Obx(
        () {
          return IndexedStack(
            index: homeController.tabIndex.value,
            children: [
              InfografikView(),
              BeritaView(),
              KonselingView(),
              ProfilView()
            ],
          );
        },
      ),
      bottomNavigationBar: Obx(
        () {
          return BottomNavigationBar(
            onTap: homeController.changeTabIndex,
            currentIndex: homeController.tabIndex.value,
            selectedItemColor: Colors.red,
            unselectedItemColor: Colors.grey,
            type: BottomNavigationBarType.fixed,
            items: const [
              BottomNavigationBarItem(
                icon: Icon(Icons.pie_chart),
                label: "Infografik",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.newspaper),
                label: "Berita",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.chat_bubble),
                label: "Konsultasi",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.account_circle),
                label: "profil",
              ),
            ],
          );
        },
      ),
    );
  }
}
