import 'package:get/get.dart';
import 'package:siga/app/modules/konseling/controllers/konseling_controller.dart';

import '../controllers/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
    Get.lazyPut<KonselingController>(
      () => KonselingController(),
    );
  }
}
