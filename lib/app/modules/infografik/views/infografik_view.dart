import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:pie_chart/pie_chart.dart';

import '../controllers/infografik_controller.dart';

class InfografikView extends GetView<InfografikController> {
  static const double _kItemExtent = 32.0;

  Map<String, double> dataMap = {
    "Non KDRT": 40,
    "KDRT": 60,
  };

  final _tahun = ["2019", "2020", "2021", "2022"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Grafik Data KtPA'),
        centerTitle: true,
        bottom: PreferredSize(
          preferredSize: const Size(0, 50),
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 50,
              vertical: 20,
            ),
            child: Row(
              children: [
                const Expanded(
                  flex: 1,
                  child: Text(
                    "Pilih Tahun",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: CupertinoTextField(
                    readOnly: true,
                    onTap: () {
                      showCupertinoModalPopup(
                        context: context,
                        builder: (BuildContext context) {
                          return Container(
                            height: Get.height * 0.4,
                            padding: EdgeInsets.only(top: 6),
                            margin: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom,
                            ),
                            color: CupertinoColors.white,
                            child: SafeArea(
                              child: CupertinoPicker(
                                magnification: 1.22,
                                squeeze: 1.2,
                                useMagnifier: true,
                                itemExtent: _kItemExtent,
                                onSelectedItemChanged: (int selectedItem) {},
                                children: List<Widget>.generate(
                                  _tahun.length,
                                  (int index) {
                                    return Center(
                                      child: Text(
                                        _tahun[index],
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      body: Container(
        color: Colors.grey.shade100,
        padding: const EdgeInsets.only(
          top: 20,
        ),
        width: Get.width,
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              width: Get.width * 0.9,
              child: Column(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                    ),
                    height: 50,
                    alignment: Alignment.center,
                    child: const Text(
                      "Grafik jumlah korban KtPA laki-laki & Perempuan",
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const Text('donut chart'),
                  const Divider(),
                  const Text("bar chart"),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
